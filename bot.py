import telebot
from telebot import types
import requests
from bs4 import BeautifulSoup as bs
import psycopg2
from sys import argv

run_command = argv

TOKEN = ''
url = 'https://ru.wikipedia.org/wiki/Городские_населённые_пункты_Московской_области'
wiki_url = 'https://ru.wikipedia.org'


con = psycopg2.connect(
    database="alef",
    user="",
    password="",
    host="127.0.0.1",
    port="5432"
)


def migrations():
    cur = con.cursor()
    cur.execute('''CREATE TABLE City
                  (Name CHARACTER VARYING (50),
                   Population INTEGER,
                   Link TEXT);''')
    cur.execute('''CREATE INDEX ON City (LOWER(Name));''')
    con.commit()


def find_close(name):
    cur = con.cursor()
    cur.execute(f'''SELECT Name, Population FROM City WHERE LOWER(Name) LIKE LOWER('%{name}%');''')
    rows = cur.fetchall()
    cities = {r[0]: r[1] for r in rows}
    return cities


def get_names_with_population():
    cur = con.cursor()
    cur.execute('''SELECT Name, Population FROM City;''')
    rows = cur.fetchall()
    cities = {r[0]: r[1] for r in rows}
    return cities


def tg_bot():
    tb = telebot.TeleBot(TOKEN)
    start_command = types.BotCommand('/start', '\U0001F680 Выбрать город')
    parse_wiki_command = types.BotCommand('/parse_wiki', '\U0001F680 Обновить данные')
    bot_commands = [start_command, parse_wiki_command]
    tb.set_my_commands(bot_commands)

    @tb.message_handler(commands=['start'])
    def show_cities(message):
        cities_names = get_names_with_population()
        markup = types.InlineKeyboardMarkup()
        for name, popul in cities_names.items():
            btn = types.InlineKeyboardButton(text=name, callback_data=f'{name}-{popul}', parse_mode='Markdown')
            markup.add(btn)
        tb.send_message(message.chat.id, 'Выберите город:', reply_markup=markup)

    @tb.message_handler(commands=['parse_wiki'])
    def parse_wiki(message):
        msj = tb.send_message(message.chat.id, '\U000023F3 Обновляю данные...', parse_mode='Markdown')
        parse_page()
        tb.delete_message(chat_id=message.chat.id, message_id=msj.message_id)
        tb.send_message(message.chat.id, 'Данные обновились', parse_mode='Markdown')

    @tb.callback_query_handler(func=lambda call: True)
    def answer(call):
        data = call.data.split('-')
        name, popul = data[0], data[1]
        tb.send_message(call.message.chat.id, f'В городе {name} проживает {popul} человек', parse_mode='Markdown')

    @tb.message_handler(content_types=['text'])
    def send_text(message):
        cities_names = find_close(message.text)
        if not cities_names:
            tb.send_message(message.chat.id, 'К сожалению, похожего по названию города не найдено:(\n'
                                             'Попробуйте еще раз:')
        else:
            markup = types.InlineKeyboardMarkup()
            for name, popul in cities_names.items():
                btn = types.InlineKeyboardButton(text=name, callback_data=f'{name}-{popul}', parse_mode='Markdown')
                markup.add(btn)
            tb.send_message(message.chat.id, 'Вот подходящие города:', reply_markup=markup)
    tb.infinity_polling()


def parse_page():
    cur = con.cursor()
    resp = requests.get(url)
    soup = bs(resp.text, 'lxml')
    a = soup.find_all('table', class_='standard sortable')
    names = []
    for i in a:
        b = i.find_all('tr')
        for r in b:
            c = r.find_all('td')
            if len(c) > 4:
                names.append(c[1].text)
                name, population, link = c[1].text, int(c[4].text[1:-10].replace(u'\xa0', '')), wiki_url + c[1].find('a').get('href')
                cur.execute(f'''SELECT * FROM City WHERE Name = '{name}';''')
                rows = cur.fetchall()
                if len(rows) < 1:
                    cur.execute(f'''INSERT INTO City
                                   (Name, Population, Link) VALUES
                                   ('{name}', {population}, '{link}');''')

                else:
                    cur.execute(f'''UPDATE City
                                    SET Name='{name}', Population = {population}, Link = '{link}'
                                    WHERE Name = '{name}';''')
                con.commit()


if __name__ == '__main__':
    ''' Use migrations only for first running of this app '''
    if 'migrate' in argv:
        migrations()
    else:
        tg_bot()
        con.close()
