# AlefTestovoe

To start using this app, firstly you should create virtual environment, than install packages from requirements.txt file with command:
```
pip install -r requirements.txt
```
Than you should create telegram bot from BotFather bot of Telegram and add TOKEN to the bot.py file.
After create database (Postgres) and add username, password and database name in the bot.py file. I reccomend you create database with options LOCALE and TEMPLATE (this is important for correctly working with Russian words/cities names):
```
CREATE DATABASE alef WITH LOCALE = 'ru_RU.UTF-8' TEMPLATE = 'template0';
```

After all for the first time you need to make migrations:
```
python bot.py migrate
```

Then run bot.py file with no argument.
